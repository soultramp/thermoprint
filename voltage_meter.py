#!/usr/bin/env python3

import adafruit_ads1x15.ads1015 as ADS
from adafruit_ads1x15.analog_in import AnalogIn
import board
import busio
from time import sleep

# Setup for the analog digital converter
#ADS1015 = 0x00  # 12-bit ADC
i2c = busio.I2C(board.SCL, board.SDA)
adc = ADS.ADS1015(i2c)

volt_meter = AnalogIn(adc, ADS.P3)

def get_voltage():
    voltage = volt_meter.voltage * 6.45
    return voltage


if __name__ == "__main__":
    print(get_voltage())
