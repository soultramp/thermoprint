. /home/pi/venv/bin/activate
cd /home/pi/thermoprint
sudo -u pi pkill -9 -f thermoprint.py
nohup python3 -u /home/pi/thermoprint/thermoprint.py > /home/pi/thermoprint.log & disown
