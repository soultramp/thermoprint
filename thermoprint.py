import os
import glob
import time
import signal
import sys
import random
import adafruit_ads1x15.ads1015 as ADS
from adafruit_ads1x15.analog_in import AnalogIn
import board
import busio
from datetime import datetime

import RPi.GPIO as GPIO


GPIO.setup(26, GPIO.OUT)
GPIO.output(26, GPIO.HIGH)

# Setup for the analog digital converter
#ADS1015 = 0x00  # 12-bit ADC
i2c = busio.I2C(board.SCL, board.SDA)
adc = ADS.ADS1015(i2c)

sharp = AnalogIn(adc, ADS.P0)
volt_meter = AnalogIn(adc, ADS.P3)

SENSOR_INTERVAL = 0.3  # Time between distance checks
WAIT_AFTER_PRINT = 15   # Time between prints
TRIGGER_DIST = 20      # Trigger distance in cm for printing
MIN_VOLTAGE = 14       # Minimum voltage for printer operation

def get_distance():
    distance = 27.242 * pow(sharp.voltage, -1.1904)
    return distance


def get_voltage():
    voltage = volt_meter.voltage * 6.3
    return voltage


def print_story():
    files = glob.glob("/var/www/uploads/unzipped/*.pdf")
    choice = random.randrange(0, len(files))
    print_order = "lp -U pi " + '"' + files[choice] + '"'
    os.system(print_order)
    print(print_order)
    time.sleep(WAIT_AFTER_PRINT)


def write_log():
    now = datetime.now()
    dt_string = now.strftime("%d.%m.%Y\n")
    with open('/var/www/html/stats.csv', 'a') as file:
        file.write(dt_string)


def run_printer():
    print("Starting thermoprint service...done!")

    while get_voltage() >= MIN_VOLTAGE:
        GPIO.output(26, GPIO.HIGH)
        if  0 <= get_distance() <= TRIGGER_DIST:
            print_story()
            write_log()
        time.sleep(SENSOR_INTERVAL)

    GPIO.output(26, GPIO.LOW)


if __name__ == "__main__":
    run_printer()
