input="/media/usb/wifi.txt"
ssid=$(head -n 1 $input)
pass=$(tail -1 $input)
sudo -u pi echo -e "ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev\nupdate_config=1\ncountry=US\n\nnetwork={\n\tssid=\""$ssid"\"\n\tpsk=\""$pass"\"\n\tkey_mgmt=WPA-PSK\n}" > /etc/wpa_supplicant/wpa_supplicant.conf
