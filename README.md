# Thermoprint

Software for the the Star TSP100 thermoprint project for Kombinat Lump.

## Connect to wifi
#### Option 1: Using a pen drive (recommended)
Create a txt file named wifi.txt containing just the ssid and passphrase of the wifi network.
Connect it to the printer and wait for the green led stop flashing.

```bash
name_of_wifi
pw_of_wifi
```

#### Option 2: Using the Raspberry Pi directly
Connect a monitor, mouse and keyboard to the printer, connect to the wifi via the gui.

## Updating the printable files
#### Option 1: Using the web interface 
Connect to the printer with your browser typing thermoprinter.local or its ip address into the address bar.
Upload a zip file containing the printables in pdf format.

#### Option 2: Using a pen drive
Put a zip file named thermoprint_pdf.zip containing your pdf files on an usb flash drive.
Plug it into an usb port of the printer.
The flashing green led indicates the printer is processing the usb drive.
Permanent green means everything went fine, red means there was an error.


