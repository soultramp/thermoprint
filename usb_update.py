from pyudev import Context, Monitor
import os
import glob
import zipfile
from time import sleep
import RPi.GPIO as GPIO
import threading

GPIO.setwarnings(False)
GPIO.setmode(GPIO.BCM)
GPIO.setup(16,GPIO.OUT)
GPIO.setup(12,GPIO.OUT)

context = Context()
monitor = Monitor.from_netlink(context)
monitor.filter_by('usb')


def blink_green(arg):
    t = threading.currentThread()
    while True:
        while getattr(t, "run", True):
            GPIO.output(12, True)
            sleep(0.5)
            GPIO.output(12, False)
            sleep(0.5)


def wait_for_drive():
    while True:
        device = monitor.poll(timeout = 3)
        if device is not None:
            if device.action == "add" and device.device_node is not None:
                blink.run = True
                print("USB drive found. Mounting...", end = "")
                sleep(3)
                os.system("bash /home/pi/thermoprint/mount_usb.sh")
                print("done!")
                sleep(1)
                return True


def wait_for_unplug():
    while True:
        device = monitor.poll(timeout = 3)
        if device is not None:
            if device.action == "remove" and device.device_node is not None:
                GPIO.output(16, False)
                GPIO.output(12, False)
                return True


def unmount():
    print("Unmounting USB drive...", end = "")
    os.system("bash /home/pi/thermoprint/umount_usb.sh")
    sleep(1)
    print("done!")


def success():
    unmount()
    blink.run = False
    sleep(1)
    GPIO.output(12, True)
    wait_for_unplug()


def update_print_files():
    print("Stopping thermoprint service...", end="")
    os.system("pkill -9 -f thermoprint.py")
    print("done!")
    files = glob.glob("/var/www/uploads/unzipped/*")
    print("Deleting old files...", end = "")
    for f in files:
        try:
            os.remove(f)
        except OSError as e:
            print(e.strerror)
            print(e.code)
    print("done!")
    zip_file = "/media/usb/thermoprint_pdf.zip"
    print("Extracting new files...", end = "")
    with zipfile.ZipFile(zip_file, "r") as zip_ref:
        zip_ref.extractall("/var/www/uploads/unzipped")
    print("done!")
    print("Restarting thermoprint service...", end = "")
    os.system("nohup python3 -u /home/pi/thermoprint/thermoprint.py > /home/pi/thermoprint.log &")
    print("done!")
    print("USB update finished successfully!")


def setup_wifi():
    print("Setting up wifi...", end = "")
    os.system("bash /home/pi/thermoprint/setup_wifi.sh")
    sleep(1)
    print("done!")


if __name__ == "__main__":
    print("Starting usb update watchdog...done!")
    blink = threading.Thread(target = blink_green, args=("task",))
    blink.start()
    blink.run = False
    while True:
        wait_for_drive()
        print("Processing USB update:")
        thermo = os.path.exists("/media/usb/thermoprint_pdf.zip")
        wifi = os.path.exists("/media/usb/wifi.txt")

        if thermo is not True and wifi is not True:
            blink.run = False
            GPIO.output(16, True)
            print("ERROR: Neither 'thermoprint_pdf.zip' nor 'wifi.txt' found in USB root directory!")
            wait_for_unplug()
            continue
        blink.run = True
        if wifi is True:
            setup_wifi()
        if thermo is True:
            update_print_files()
        success()
